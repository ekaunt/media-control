add a media controller to your bar/panel/dock!

## Installation
### dependencies
 - [playerctl](https://github.com/acrisci/playerctl)
 - [fontawesome](https://fontawesome.com/)

### polybar
place the script in a folder u want it, and add this to ur polybar config (`~/.config/polybar/config`)
```
[module/mediacontrol]
type = custom/script
exec = /path/to/files/mediacontrol main
click-right = /path/to/files/mediacontrol play/pause
click-left = /path/to/files/mediacontrol play/pause
click-middle = playerctl --all-players pause
scroll-up = /path/to/files/mediacontrol prev
scroll-down = /path/to/files/mediacontrol next
```
dont forget to add the module to the bar 😉

### tint2
place the script in a folder u want it, and add this to ur tint2 config (`~/.config/tint2/tint2rc`)
```
#-------------------------------------
# Executor 1
execp = new
execp_command = /path/to/files/mediacontrol main
execp_interval = 1
execp_has_icon = 0
execp_cache_icon = 0
execp_continuous = 2
execp_markup = 0
execp_lclick_command = /path/to/files/mediacontrol play/pause
execp_rclick_command = /path/to/files/mediacontrol play/pause
execp_mclick_command = playerctl --all-players pause
execp_uwheel_command = /path/to/files/mediacontrol prev
execp_dwheel_command = /path/to/files/mediacontrol next
execp_font = Font Awesome 5 Free Heavy 11
execp_font_color = #ffffff 100
execp_padding = 3 0
execp_background_id = 0
execp_centered = 1
execp_icon_w = 0
execp_icon_h = 0
```

## configuration
| name           | values             | description                                                    |
|----------------|--------------------|----------------------------------------------------------------|
| `ICON_PLAY`    | `string`           | a character to show when the player is playing                 |
| `ICON_PAUSE`   | `string`           | a character to show when the player is paused                  |
| `ONE_LINE`     | `"true"`/`"false"` | show the status as one or two lines (useful if using polybar)                            |
| `SHOW_NUMBERS` | `"true"`/`"false"` | show which player out of all open players is currently playing |

## extras
u can also add a keyboard shortcut like ↓ (sxhkd example) to toggle play/pause from anywhere in the player that u selected from the bar/panel
```
super + p
    /path/to/script/mediacontrol play/pause
```
