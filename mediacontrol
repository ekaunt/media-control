#!/bin/bash
#set -x
ICON_PLAY=""
ICON_PAUSE=""
ONE_LINE="false"
SHOW_NUMBERS="true"

#set which player is currently playing (if any)
if [ -f "$HOME"/.config/current-player ]; then
	player=$(cat "$HOME"/.config/current-player)
fi

# remove chrome as a player if u have plasma-browser-integration installed
if grep -q "^plasma-browser" <<< "$(playerctl -l)"; then
	players="$(playerctl -l | sed -e /^chrom.*/d)"
else
	players="$(playerctl -l)"
fi

if [ -z "$player" ] || ! grep -q "^$player" <<< "$(playerctl -l)"; then
	player=$(echo "$players" | sed -n "1p")
fi

# get the line number of the current player from the list of all players
line_number=$(echo "$players" | grep -in "^$player$" | cut -d: -f 1)
#remove stopped players from the list of all players
for line in "$players"; do
	if ! grep -q "Stopped" <<< "$(playerctl --player="$line" status 2>&1)"; then
		printf -v active_players "${active_players}${line}"
	fi
	printf -v active_players "${active_players}\n"
done
#echo "$active_players"
active_players="${active_players::-1}"
lines="$(echo "$active_players" | wc -l)"
#echo "$active_players" | wc -l
#echo "$active_players"

# write the current playing media player into a file for other programs (& this one too) to use
function save-player(){
	if [ "$line_number" -gt "$lines" ]; then
		line_number=1
	elif  [ "$line_number" -lt 1 ]; then
		line_number="$lines"
	fi
	#player=$(playerctl -l | grep -in "$player" | cut -d: -f 2)
	player=$(playerctl -l | sed -e /^chrom.*/d | sed -n "${line_number}p")
	echo "$player" > "$HOME"/.config/current-player
}

case $1 in
	-h|--help|h|help)
		echo -e "USAGE: mediacontrol {next|prev}\n"
		echo -e "\t*\t\ttoggles play/pause the playing video"
		echo -e "\tnext\t\tchanges the controller to the next video"
		echo -e "\tprev\t\tchanges the controller to the previous video"
		echo -e "\tnext-track\t\tchanges the playing video/song to the next one"
		echo -e "\tprev-track\t\tchanges the playing video/song to the previous one"
		echo -e "\tplay/pause\t\ttoggles the chosen video player to play or pause"
		echo -e "\tmain\t\tprints a play/pause icon for use in a bar/panel/dock"
		echo -e "\thelp\t\tshows this help message"
		;;
	next)
		echo "$line_number"
		((line_number++))
		echo "$line_number"
		save-player
		;;
	prev)
		((line_number--))
		save-player
		;;
	next-track)
		playerctl --player="$player" next
		;;
	prev-track)
		playerctl --player="$player" previous
		;;
	play/pause )
		playerctl --player="$player" play-pause
		;;
	play )
		playerctl --player="$player" play
		;;
	pause )
		playerctl --player="$player" pause
		;;
	main )
		# 2>&1 is cuz playerctl outputs to stderr and if only reads stdout, so it redirects it
		status="$(playerctl --player="$player" status 2>&1)"
		#if [ "$(playerctl -l 2>&1)" = "No players were found" -o "(grep -q "Stopped" <<< "$status" -a  "$lines" -eq 1 )" ]; then
		if (grep -q "Stopped" <<< "$status" && [ "$lines" -eq 1 ] ) || [ "$(playerctl -l 2>&1)" = "No players were found" ]; then
			echo -e "\n"
		else
			if [[ "$status" == "Playing" ]]; then
				ICON="$ICON_PLAY"
			elif [[ "$status" == "Paused" ]]; then
				ICON="$ICON_PAUSE"
			fi
			if [ "$ONE_LINE" = "true" ]; then
				echo -n "$ICON "
			else
				echo "$ICON"
			fi
			if [ "$SHOW_NUMBERS" = "true" -a ! -z "$ICON" ]; then
				#echo "<span size='x-small'>$line_number/$lines</span>"
				echo "$line_number/$lines"
			else
				echo
			fi
		fi
		if [ "$ONE_LINE" = "true" ]; then
			echo
		fi
		export CURRENT_PLAYER=$(playerctl -l | sed -n "1p")
		;;
esac

# display extra info in the tooltip
# it works by redirecting output to stderr (>&2), which is what gets displayed in the tooltip
#>&2 playerctl --player="$player" metadata --format '{{playerName}}: {{uc(status)}} — {{title}}'
#>&2 playerctl --player="$player" metadata --format '[{{duration(position)}}|{{duration(mpris:length)}}]'
prog() {
    local p=$1 w=$2 length=$3;  shift
    # create a string of spaces, then change them to dots
    printf -v dots "%*s" "$(( $p*$w/$length ))" ""; dots=${dots// /\-};
    # print those dots on a fixed-width space plus the percentage etc.
		printf "[%-*s] %3d%% %s" "$w" "$dots" "$((p*100/length))";
	echo
}
duration=$(playerctl --player="$player" metadata --format '{{duration(position)}}/{{duration(mpris:length)}}')
length=$(($(playerctl --player="$player" metadata --format '{{mpris:length}}')/1000000))
position=$(($(playerctl --player="$player" metadata --format '{{position}}')/1000000))
info="$(playerctl --player="$player" metadata --format '{{playerName}} ┼ {{artist}} - {{album}} - {{title}}')"
width="$((${#info} + 8))"
>&2 echo "$info"
>&2 echo -n " $duration ";
if [ "$ICON" = "" ]; then echo -n "⯈ "; else echo -n "❚❚  "; fi >&2
>&2 prog $position $width $length
